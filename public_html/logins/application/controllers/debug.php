<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debug extends CI_Controller
{
    function __construct() {
		parent::__construct();
		$this->load->helper(array('date', 'form'));
		$this->load->library(array('mail', 'email'));
		$this->load->model(array('debug_model','auth_model', 'account_manager_model', 'project_model', 'interview_model', 'job_profile_model', 'candidate_model', 'credit_model'));
	
	}
    public function index()
    {  
        $options = [
            'cost' => 12,
        ];

        $data = array();
        $data['DB_DATA'] = $this->debug_model->getManagerEmailByID(23);
        $data['all_auth'] = $this->debug_model->getAllAuths();
        $auth_data  = $this->debug_model->getAllAuths();
    
        foreach ($data['all_auth']  as $project){
            $decoded_password =  base64_decode($project->password);
            $hashed_password = password_hash($decoded_password, PASSWORD_BCRYPT);
            echo $project->email."  ".$decoded_password." "." :::: ".$hashed_password."<br>"; 
        }
       
         $this->load->view('debug',$data);
   
    }
    public function compare($incoming_password){
        $hashed_password = password_hash('yqecpubsnm', PASSWORD_BCRYPT);


        echo($hashed_password ."<br>");
     if (password_verify('yqecpubsnm', '$2y$10$rFQVYftWKsanlk5P7M0VYOPF0BaGeIcC2h1iuigcgaP9Y8BoGMKo6')) {
         echo 'Password is valid!';
     } else {
         echo 'Invalid password.';
     }

  
    }
    public function  testUpdate(){
        $auth_data  = $this->debug_model->getAllAuths();
        $data = array();
        foreach($auth_data as $key){
            $password_enc = $key->password;
            $password_clr = base64_decode($password_enc);
            $hashed_password  = password_hash($password_clr, PASSWORD_BCRYPT);
            $data[$key]['password'] = $hashed_password;
            $data[$key]['email'] =  $key->email;
            $data[$key]['type']  = $key->type;
            $data[$key]['name']  = $key->name;

          
        
            //echo $hashed_password."  <br> ";
        }
        print_r(sizeof($data));

        $response = $this->debug_model->updateALL($data);
        echo $response;
        return $data;
    }

    public function display($data){

        foreach($data as $key => $value){
            echo "{$key} => {$value} ";
            print_r($data);

        }
    }
    
}