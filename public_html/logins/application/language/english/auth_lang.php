<?php
// Auth Manager
$lang['manager_heading_title']  	  = 'Administrator';
$lang['manager_text_auth']  	      = 'Administrator List';
$lang['manager_text_empty']  		  = 'Administrator List is Empty!!';

$lang['manager_button_create'] 		  = 'Create New Administrator';

$lang['create_hr_heading_title']  	  = 'Create HR';
$lang['text_create_hr'] 			  = 'Create HR';

$lang['create_manager_heading_title'] = 'Create Administrator';
$lang['text_create_manager'] 		  = 'Create Administrator';
$lang['error_name'] 				  = 'Enter Valid Auth Name!!';
$lang['error_email'] 				  = 'Enter Valid Auth Email!!';
$lang['error_password'] 			  = 'Enter Valid Auth Password!!';

// Edit
$lang['edit_hr_heading_title']  	  = 'Edit HR';
$lang['text_edit_hr_auth'] 			  = 'Edit HR';

$lang['edit_manager_heading_title']   = 'Edit Administrator';
$lang['text_edit_manager_auth'] 	  = 'Edit Administrator';