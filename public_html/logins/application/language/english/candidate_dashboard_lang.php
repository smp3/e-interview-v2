<?php

$lang['heading_title_welcome'] 		= 'Welcome to your interview';
$lang['heading_title_my_interview'] = 'My e-interview';
$lang['text_my_interview']    		= 'My e-interview';

$lang['entry_project_name']			= 'Project Name';
$lang['entry_job_title']  			= 'Interview Guide Title';
$lang['entry_job_role_title']  		= 'Job Title';
$lang['entry_start_date']  			= 'Project Start';
$lang['entry_end_date']    			= 'Project End';
$lang['entry_action']    			= 'Action';

$lang['button_begin'] 				= 'Begin Interview';
$lang['button_watch'] 				= 'Watch Interview';