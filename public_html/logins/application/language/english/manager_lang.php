<?php

$lang['heading_title']  	 = 'Administrator';
$lang['text_manager']  		 = 'Administrator List';
$lang['text_empty']  		 = 'Administrator List is Empty!!';

$lang['label_manager_id']  	 	 = 'ID';
$lang['label_manager_name']       = 'Name';
$lang['label_manager_department'] = 'Branch / Department';
$lang['label_manager_email']      = 'Email';
$lang['label_manager_status']      = 'Status';
$lang['label_action']    	 = 'Action';

$lang['text_create']    	 = 'Create New Administrator';
$lang['text_edit']    	 	 = 'Edit Administrator';
$lang['entry_name']    	 	 = 'Full Name';
$lang['entry_department']    = 'Branch / Department';
$lang['entry_email']    	 = 'Email';
$lang['entry_status']    	 = 'Status';
$lang['entry_action']    	 = 'Action';

$lang['button_create'] 		 = 'Create New Administrator';
$lang['button_edit'] 		 = 'Edit';
$lang['button_remove'] 		 = 'Remove';
$lang['button_save'] 		 = 'Save';
$lang['button_cancel'] 		 = 'Cancel';
$lang['button_generate'] 	 = 'Generate';

$lang['error_name'] 	 	 = 'Enter Valid Administrator Name!!';
$lang['error_email'] 	 	 = 'Enter Valid Email Address';