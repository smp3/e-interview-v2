<div class="main">
	<div class="main-content">
		<div class="container-fluid">
		<h3><?= $text_heading_dashboard ?></h3>
		<div class="row">
		<?php 
						if($this->session->userdata['user_type'] === 'ADMIN' || $this->session->userdata['user_type'] === 'DISTRIBUTOR' || $this->session->userdata['user_type'] === 'ACCOUNT MANAGER') { 
							$class = 'col-md-3';
						}else if($this->session->userdata['user_type'] === 'MANAGER'){
							$class = 'col-md-3';
						}else if($this->session->userdata['user_type'] === 'RATER'){
							$class = 'col-md-6';
						}
						
						if($this->session->userdata['user_type'] === 'ACCOUNT MANAGER') { 
							$hover_note = 'data-toggle="tooltip" data-placement="top" title="Administrators are users of the system that will set up and manage the whole interview process"';
							$hover_note1 = 'data-toggle="tooltip" data-placement="top" title="All projects in entire company, as set up by all administrators. As account manager you can get an overview of all the projects and view all the interviews"';
						}else if($this->session->userdata['user_type'] === 'RATER'){
							$hover_note = 'data-toggle="tooltip" data-placement="top" title="Hooray!"';
							$hover_note1 = 'data-toggle="tooltip" data-placement="top" title="Hooray!"';
						}else if($this->session->userdata['user_type'] === 'MANAGER'){
							$hover_note = 'data-toggle="tooltip" data-placement="top" title="The raters you add will form part of your interview panel list. For every project you set up, you are able to add a specific set of raters that will only be rating the candidates in that project"';
							$hover_note1 = 'data-toggle="tooltip" data-placement="top" title="Interview guides are the set of questions associated to a specific job you are interviewing for"';
							$hover_note2 = 'data-toggle="tooltip" data-placement="top" title="The list of projects you set up as administrator. Each project will consist of an interview guide, candidates and raters"';
						}else if($this->session->userdata['user_type'] === 'RATER'){
							$hover_note = 'data-toggle="tooltip" data-placement="top" title="Hooray!"';
						}
						
						?>


<?php if($this->session->userdata['user_type'] === 'ADMIN' || $this->session->userdata['user_type'] === 'DISTRIBUTOR' || $this->session->userdata['user_type'] === 'ACCOUNT MANAGER' || $this->session->userdata['user_type'] === 'MANAGER') { ?>
	<div class="headline <?= $class ?>">
	<div class="panel">
			<div class="panel-body" style="text-align: center;">
			<a href="<?= $link_box1 ?>" <?= $hover_note ?>><span class="icon"><img src="<?= base_url() ?>assets/img/icon/Wamly_Dash/Group 278.svg" style="padding-top:10px;padding-bottom:10px;" ></span></a>							
				<p class="card-number"><?= count($box1) ?></p>
				<p class="card-title"><?= $text_total_box1 ?></p>
		</div>
	</div>
	</div>
<?php } ?>

<?php if($this->session->userdata['user_type'] === 'ADMIN' || $this->session->userdata['user_type'] === 'DISTRIBUTOR' || $this->session->userdata['user_type'] === 'ACCOUNT MANAGER' || $this->session->userdata['user_type'] === 'MANAGER') { ?>
	<div class="headline <?= $class ?>">
	<div class="panel">
			<div class="panel-body" style="text-align: center;">
			<a href="<?= $link_box2 ?>" <?= $hover_note1 ?>><span class="icon"><img src="<?= base_url() ?>assets/img/icon/Wamly_Dash/Group 279.svg" style="padding-top:10px;padding-bottom:10px;" ></span></a>							
				<p class="card-number"><?= count($box2) ?></p>
				<p class="card-title"><?= $text_total_box2 ?></p>								
		</div>
	</div>
	</div>
<?php } ?>
		
<?php if($this->session->userdata['user_type'] === 'MANAGER' || $this->session->userdata['user_type'] === 'RATER') { ?>
	<div class="headline <?= $class ?>">
	<div class="panel">
			<div class="panel-body" style="text-align: center;">
			<a href="<?= $link_box3 ?>" <?= $hover_note2 ?>><span class="icon"><img src="<?= base_url() ?>assets/img/icon/Wamly_Dash/Group 280.svg" style="padding-top:10px;padding-bottom:10px;" ></span></a>							
				<p class="card-number"><?= count($box3) ?></p>
				<p class="card-title"><?= $text_total_box3 ?></p>								
		</div>
	</div>
	</div>
<?php } ?>

<?php if($this->session->userdata['user_type'] === 'MANAGER' || $this->session->userdata['user_type'] === 'RATER') { ?>
	<div class="headline <?= $class ?>">
	<div class="panel">
			<div class="panel-body" style="text-align: center;">
			<a href="Javascript:void(0)"><span class="icon"><img src="<?= base_url() ?>assets/img/icon/Wamly_Dash/Group 280.svg" style="padding-top:10px;padding-bottom:10px;" ></span></a>
            <?php $total_credit = isset($user_info['profile']['credits']) ? $user_info['profile']['credits'] : 0 ?>
			<p class="card-number"><?= $total_credit ?></p>
			<p class="card-title"><?= $this->lang->line('Credit') ? $this->lang->line('Credit') : 'Credit'?></p>								
		</div>
	</div>
	</div>
<?php } ?>

</div>

		</div>
	</div>
</div>
<div class="clearfix"></div>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>